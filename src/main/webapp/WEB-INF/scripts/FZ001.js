/* Fuzzy Search */
function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var words = params.get("words");
    var fs = params.get("fields");
    var point = params.get("point");
    sb.append("{\n");
    sb.append("\"query\": {\n");
    sb.append("\"multi_match\": {\n");
    sb.append("\"query\": \"");
    for(var i = 0; i < words.length; i++) {
        if( i > 0 ) {
            sb.append(" ");
        }
        sb.append(words[i]);
    }
    sb.append("\",\n");
    sb.append("\"fields\": [");
    for(var i = 0; i < fs.length; i++) {
        if( i > 0 ) {
            sb.append(", ");
        }
        sb.append("\"").append(fs[i]).append("\"");
    }
    sb.append("],\n");
    if( point === null ) {
        sb.append("\"fuzziness\": \"AUTO\"\n");
    } else {
        sb.append("\"fuzziness\": \"").append(point).append("\"\n");
    }
    sb.append("}\n");
    sb.append("},\n");
    sb.append("\"_source\": [");
    for(var i = 0; i < fs.length; i++) {
        if( i > 0 ) {
            sb.append(", ");
        }
        sb.append("\"").append(fs[i]).append("\"");
    }
    sb.append("]\n");
    sb.append("}\n");
    return sb.toString();
}

/*
 * {
    "query": {
        "multi_match" : {
            "query" : "yyyy218 xxxx217",
            "fields": ["$cglib_prop_firstName", "$cglib_prop_lastName", "$cglib_prop_address"],
            "fuzziness": "AUTO"
        }
    },
    "_source": ["$cglib_prop_firstName", "$cglib_prop_lastName"]
}
 * 
 * 
 * 
 * 
 * 
 * 
 */
