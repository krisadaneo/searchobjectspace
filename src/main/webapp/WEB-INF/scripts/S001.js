function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var words = params.get("words");
    var fs = params.get("fields");
    sb.append("{\n");
    sb.append("\"query\": {\n");
    sb.append("\"bool\": {\n");
    sb.append("\"should\": [");
    for (var i = 0; i < words.length; i++) {
        if (i > 0) {
            sb.append(", ");
        }
        sb.append("{\n");
        sb.append("\"multi_match\" : {\n");
        sb.append("\"query\": \"*").append(words[i]).append("*\",");
        sb.append("\"type\": \"cross_fields\",\n");
        sb.append("\"fields\": [");
        for (var j = 0; j < fs.length; j++) {
            if (j > 0) {
                sb.append(", ");
            }
            sb.append("\"").append(fs[j]).append("\"");
        }
        sb.append("],\n\"minimum_should_match\": \"10%\" \n");
        sb.append("}\n");
        sb.append("}\n");
    }
    sb.append("]\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    return sb.toString();
}
