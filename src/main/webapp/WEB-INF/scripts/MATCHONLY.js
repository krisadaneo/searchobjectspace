/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var word = params.get("word");
    var field = params.get("field");
    sb.append("{\n");
    sb.append("\"query\": { \"match_phrase\": { ");
    sb.append("\"$cglib_prop_").append(field).append("\": \"").append(word).append("\" } }");
    sb.append("}\n");
    print("JS:"+ sb.toString());
    return sb.toString();
}

