/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var group = params.get("sanction_group");
    
    var status = params.get("status");
    sb.append("{\n");
    sb.append("\"query\": { \"bool\": { \n");
    sb.append("\"must\": [  \n");
    sb.append("{\n");
    sb.append("\"match\": { \n");
    sb.append("\"$cglib_prop_").append("sanctionGroup").append("\": \"").append(group).append("\" } }, \n");
    sb.append("{\n");
    sb.append("\"match\": { \n");
    sb.append("\"$cglib_prop_").append("status").append("\": \"").append(status).append("\" } } \n");
    sb.append("]\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    print("JS:"+ sb.toString());
    return sb.toString();
}

/*
 {
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "$cglib_prop_sanctionGroup": "High-Risk-Turkey"
          }
        },
        {
          "match": {
            "$cglib_prop_status": "INACTIVE"
          }
        }
      ]
    }
  }
}
 
 
 */