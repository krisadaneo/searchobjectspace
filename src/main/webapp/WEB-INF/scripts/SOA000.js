function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var orwords = params.get("orwords");
    var andwords = params.get("andwords");
    var ofs = params.get("orfields");
    var afs = params.get("andfields");
    sb.append("{\n");
    sb.append("\"query\": {\n");
    sb.append("\"bool\": {\n");
    sb.append("\"must\": {");
    sb.append("\"bool\": {\n");
    var flag = false;
    if (ofs !== null) {
        if (ofs.size() > 0) {
            sb.append("\"should\": [\n");
            for (var i = 0; i < ofs.size(); i++) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append("{\"match\":{\"$cglib_prop_").append(ofs.get(i)).append("\": \"").append(orwords.get(i)).append("\"}}\n");
            }
            sb.append("]\n");
            flag = true;
        }
    }
    if (afs !== null) {
        print("pass1");
        if (afs.size() > 0) {
            print("pass2");
            if (flag === true) {
                print("pass3");
                sb.append(",\n");
            }
            print("pass4");
            sb.append("\"must\":[\n");
            for (var i = 0; i < afs.size(); i++) {
                if (i > 0) {
                    sb.append(",");
                }
                print("pass");
                sb.append("{\"match\":{").append("\"$cglib_prop_").append(afs.get(i)).append("\":\"").append(andwords.get(i)).append("\"}}\n");
            }
            sb.append("]");
            print("pass6");
        }
        print("pass7");
    }
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    return sb.toString();
}

/*
 {
 "query": {
 "bool": {
 "must": {
 "bool" : { 
 "should": [
 { "match": { "title": "Elasticsearch" }},
 { "match": { "title": "Solr" }} 
 ],
 "must": { "match": { "authors": "clinton gormely" }} 
 }
 }
 }
 }
 }*/