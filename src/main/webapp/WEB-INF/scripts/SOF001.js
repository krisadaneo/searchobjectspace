function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var orwords = params.get("orwords");
    var andwords = params.get("andwords");
    var ofs = params.get("orfields");
    var afs = params.get("andfields");
    var word = "";
    sb.append("{\n");
    sb.append("\"query\": {\n");
    sb.append("\"bool\": {\n");
    sb.append("\"must\": {");
    sb.append("\"bool\": {\n");
    if (orwords !== null) {
        print("Type:" + orwords.getClass().getName());
        if (orwords.getClass().getName().equals("java.util.ArrayList")) {
            for (var i = 0; i < orwords.size(); i++) {
                if (i > 0) {
                    word = word + " ";
                }
                word = word + orwords.get(i);
            }
            print("Array:" + word);
        } else {
            word = orwords;
            print("word:" + word);
        }
    }
    var flag = false;
    sb.append("\"should\": [\n");
    if (ofs !== null) {
        if (ofs.size() > 0) {
            sb.append("{\n");
            sb.append("\"multi_match\":{\n");
            sb.append("\"query\" : \"").append(word).append("\",\n");
            sb.append("\"fields\": [");
            for (var i = 0; i < ofs.size(); i++) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append("\"$cglib_prop_upc").append(ofs.get(i)).append("\"");
            }
            sb.append("],\"fuzziness\": \"AUTO\"}\n");
            sb.append("}\n");
            flag = true;
        }
    }
    if (afs !== null) {
        if (afs.size() > 0) {
            if (flag === true) {
                if (ofs.size() > 0) {
                    sb.append(",\n");
                }
            }
            for (var i = 0; i < afs.size(); i++) {
                if (i > 0) {
                    sb.append(",");
                }
                sb.append("{\"match\": { \"$cglib_prop_upc").append(afs.get(i)).append("\": \"").append(andwords.get(i)).append("\"}}\n");
            }
        }
    }
    sb.append("]\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    return sb.toString();
}
/*
 {
 "query": {
 "bool": {
 "must": {
 "bool": {
 "should": [
 {
 "multi_match":{
 "query" : "CBSF SFA US",
 "fields": ["$cglib_prop_goodscCode1", "$cglib_prop_goodscCode2", "$cglib_prop_goodscCode3"],
 "fuzziness": "AUTO"
 }
 },
 {
 "match": { "$cglib_prop_id": "61338" }
 }
 ]                
 }
 }
 }
 }
 }*/