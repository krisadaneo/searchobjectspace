function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var words = params.get("words");
    var fs = params.get("fields");
    sb.append("{\n");
    sb.append("\"query\": {\n");
    sb.append("\"query_string\": {\n");
    sb.append("\"query\": \"*").append(words[0]).append("*\",\n");
    sb.append("\"fields\": [");
    for(var i = 0; i < fs.length; i++) {
        if( i > 0 ) { sb.append(", "); }
        sb.append("\"").append(fs[i]).append("\"");
    }
    //$cglib_prop_firstName", "$cglib_prop_surname"
    sb.append("]\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    return sb.toString();
}
