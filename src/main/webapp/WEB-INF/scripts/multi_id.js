/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var words = params.get("words");
    var fs = params.get("fields");
    var id = params.get("id");
    var fieldId = params.get("field_id");
    var dist = params.get("dist");
    if (dist === null) {
        dist = 2;
    }
    sb.append("{\n");
    sb.append("\"query\": {");
    sb.append("\"bool\": {\n");
    sb.append("\"should\": [ {\n");
    sb.append("\"query_string\": {\n");
    sb.append("\"query\": \"").append(id).append("\",\n");
    sb.append("\"fields\": [\"").append(fieldId).append("\"]}}\n");
    sb.append("],\n");
    sb.append("{\n");
    sb.append("\"multi_match\": {\n");
    sb.append("\"query\": \"");
    for (var i = 0; i < words.length; i++) {
        if (i > 0) {
            sb.append(" ");
        }
        sb.append(words[i]);
    }
    sb.append("\",\n");
    sb.append("\"fuzziness\": \"").append(dist).append("\",\n");
    sb.append("\"fields\": [");
    for (var i = 0; i < fs.length; i++) {
        if (i > 0) {
            sb.append(",");
        }
        sb.append("\"").append(fs[i]).append("\"");
    }
    sb.append("]\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    return sb.toString();
}