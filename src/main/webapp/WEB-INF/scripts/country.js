/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var words = params.get("words");
    var fs = params.get("fields");
    sb.append("{\n");
    sb.append("\"query\": {\n");
    sb.append("\"bool\": {\n");
    sb.append("\"should\": [");
    for (var i = 0; i < words.length; i++) {
        if (i > 0) {
            sb.append(", ");
        }
        sb.append("{\"query_string\": {\n");
        sb.append("\"query\": \"").append(words[i]).append("\",\n");
        sb.append("\"fields\": [\"$cglib_prop_").append(fs[i]).append("\"]");
        sb.append("}\n");
        sb.append("}\n");
    }
    sb.append("]\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    return sb.toString();
}


