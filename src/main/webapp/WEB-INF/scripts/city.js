function execute(params) {
    var StringBuilder = Java.type('java.lang.StringBuilder');
    var sb = new StringBuilder();
    var words = params.get("words");
    var fs = params.get("fields");
    sb.append("{\n");
    sb.append("\"query\": {\n");
    sb.append("\"bool\": {\n");
    sb.append("\"should\": [");
    for (var i = 0; i < words.length; i++) {
        if (i > 0) {
            sb.append(", ");
        }
        sb.append("{\"query_string\": {\n");
        sb.append("\"query\": \"").append(words[i]).append("\",\n");
        sb.append("\"fields\": [\"").append(fs[i]).append("\"]");
        sb.append("}\n");
        sb.append("}\n");
    }
    sb.append("]\n");
    sb.append("}\n");
    sb.append("}\n");
    sb.append("}\n");
    return sb.toString();
}
/*
 * {
  "query": {
    "bool": {
      
      "should": [
        {
          "query_string": {
            "query": "8",
            "fields": ["$cglib_prop_personId"]
          }
        }
        ,{
          "multi_match": {
            "query": "yyy 8",
            "fuzziness": "2",
            "fields": ["$cglib_prop_firstName"]
          }
        }
      ]
    }
  }
}
 * 
 */
        

