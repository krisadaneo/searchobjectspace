/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.searchobjectspace;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author krisada
 */
@RestController
public class SearchController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private Environment env;

    @Autowired
    private DBService dbService;

    @RequestMapping(value = "/searchMatch",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> execute(@RequestBody Map<String, Object> payload) {
        JSONObject resp = new JSONObject();
        resp.put("code", 200);
        resp.put("message", "success");
        try {
            if (SearchService.cache.isEmpty()) {
                dbService.loadNoiseWords();
                for (String s : SearchService.cache) {
                    log.info("noise:" + s);
                }
            }
            Hashtable params = new Hashtable();
            String indexCoordinate = (String) payload.get("index");
            String templ = (String) payload.get("templ");
            if (env.containsProperty("noise.skip." + templ)) {
                Iterator its = payload.keySet().iterator();
                while (its.hasNext()) {
                    String key = (String) its.next();
                    params.put(key, payload.get(key));
                }
            } else {
                Iterator its = payload.keySet().iterator();
                while (its.hasNext()) {
                    String key = (String) its.next();
                    //log.info("Key:" + key);
                    if (env.containsProperty("find." + key)) {
                        Object o = payload.get(key);
                        //log.info("o:" + o.getClass().getName());
                        if (o.getClass().getName().equals("java.util.ArrayList")) {
                            List ls = (List) payload.get(key);
                            List ltk = new ArrayList();
                            for (int i = 0; i < ls.size(); i++) {
                                log.info("CHECK WORD[" + ls.get(i) + "]");
                                String token = ((String) ls.get(i)).toUpperCase();
                                //log.info("TOKEN FOUNDED [" + ls.get(i) + "]");
                                //log.info("TOKEN FOUNDED2 [" + token + "]");
                                if (token != null) {
                                    for (String p : SearchService.cache) {
                                        //log.info("lsst P :[" + p + "]");
                                        if (p != null) {
                                            token = token.replace("{", "");
                                            token = token.replace("}", "");
                                            if (token.contains(p.trim())) {
                                                log.info("IN REPLACE token[" + token + "]1[" + p.trim() + "]");
                                                if (env.containsProperty("escape_" + p.trim())) {
                                                    token = token.replaceAll("\\" + p.trim(), "");
                                                } else {
                                                    token = token.replaceAll(p.trim(), "");
                                                }
                                                //log.info("TOKEN WITH REPLACE [" + token + "]");
                                            }
                                        }
                                    }
                                }
                                //log.info("token:" + token);
                                ltk.add(token.trim());
                            }
                            //log.info("-Key:" + key + "");
                            params.put(key, ltk);
                        } else {
                            String token = ((String) payload.get(key)).toUpperCase();
                            if (token != null) {
                                for (String p : SearchService.cache) {
                                    if (p != null) {
                                        if (token.contains(p.trim())) {
                                            //log.info("IN REPLACE 2[" + p.trim() + "]");
                                            if (env.containsProperty("escape_" + p.trim())) {
                                                token = token.replaceAll("\\" + p.trim(), "");
                                            } else {
                                                token = token.replaceAll(p.trim(), "");
                                            }
                                        }
                                    }
                                }
                            }
                            params.put(key, token.trim());
                        }
                    } else {
                        params.put(key, payload.get(key));
                    }
                }
            }
            for (Enumeration enums = params.keys(); enums.hasMoreElements();) {
                String key = (String) enums.nextElement();
                Object ov = params.get(key);
                if (ov.getClass().getName().equals("java.util.ArrayList")) {
                    log.info("Key:" + key + ", value:");
                    List lv = (List) ov;
                    for (int i = 0; i < lv.size(); i++) {
                        log.info("MATCHLIST :" + (String) lv.get(i) + ",");

                    }
                } else {
                    log.info("Key:" + key + ", value:" + (String) ov);
                }
            }
            log.info(String.format("host:%s", env.getProperty("elasticsearch.host")));
            File fileScript = new File(servletContext.getRealPath("/WEB-INF/scripts/" + templ + ".js"));
            ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
            engine.eval(new FileReader(fileScript));
            Invocable invocable = (Invocable) engine;
            String json = (String) invocable.invokeFunction("execute", params);
            log.info("JSON:" + json);
            String strJson = ElasticSearchClient.client().request(
                    env.getProperty("elasticsearch.host"), indexCoordinate, json);
            log.info("Response : " + strJson);
            boolean contain = false;
            Hashtable cp = new Hashtable();
            if (params.containsKey("andfields")) {
                contain = true;
                List lp = (List) params.get("andfields");
                List wp = (List) params.get("andwords");
                for (int i = 0; i < lp.size(); i++) {
                    String f = (String) lp.get(i);
                    String w = (String) wp.get(i);
                    cp.put(String.format("$cglib_prop_upc%s",f), w);
                }
            }
            JSONArray rss = new JSONArray();
            JSONObject jsonTrans = new JSONObject(strJson);
            JSONObject _hits = jsonTrans.getJSONObject("hits");
            /*
            int totalValue = _hits.getJSONObject("total").getInt("value");
            if( totalValue > 0 ) {
               double maxScore = _hits.getDouble("max_score");
            }*/
            JSONArray hits = _hits.getJSONArray("hits");
            for (int i = 0; i < hits.length(); i++) {
                JSONObject os = hits.getJSONObject(i);
                JSONObject obj = os.getJSONObject("_source");
                boolean cid = false;
                double score = os.getDouble("_score");
                log.info(obj.toString());
                Map omp = obj.toMap();
                JSONObject nObj = new JSONObject();
                nObj.put("_score", score);
                Iterator ks = omp.keySet().iterator();
                while (ks.hasNext()) {
                    String k = (String) ks.next();
                    /*if (contain) {
                        if (cp.containsKey(k)) {
                            String vp = (String) omp.get(k);
                            String wp = (String) cp.get(k);
                            if (wp.equals(vp)) {
                                cid = true;
                            }
                        }
                        log.info("field:" + getFieldName(k));
                        nObj.put(getFieldName(k), omp.get(k));

                    } else {
                        log.info("field:" + getFieldName(k));
                        nObj.put(getFieldName(k), omp.get(k));
                    }*/
                    nObj.put(getFieldName(k), omp.get(k));
                }
                /*if (contain == true) {
                    if (cid == true) {
                        rss.put(nObj);
                    }
                } else {
                    rss.put(nObj);
                }*/
                rss.put(nObj);
            }
            resp.put("data", rss);
            log.info("Transaction Success..,");
        } catch (Exception ex) {
            resp.put("code", 500);
            resp.put("message", String.format("Transaction error:%s", ex.getMessage()));
            log.error("Error:", ex);
            ex.printStackTrace();
        }
        return new ResponseEntity<String>(resp.toString(), HttpStatus.OK);
    }

    private String getFieldName(String fname) {
        return fname.substring(fname.lastIndexOf("_") + 1, fname.length());
    }
}
