/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.searchobjectspace;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author krisada
 */
public class SearchService {

    public static List<String> cache = new ArrayList<String>(); // GLOBAL VARIABLE

    public static boolean inCache(String cache) {
        return cache.contains(cache);
    }

    // Put data in global cache variable
    public static void addCache(String key) {
        cache.add(key);
    }
}
