/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.searchobjectspace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author krisada
 */
@Service
public class DBServiceImpl implements DBService {

    @Autowired
    private RepositoryService repository;
    
    @Override
    public void loadNoiseWords() {
        repository.findNoiseword();
    }
    
}
