/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.searchobjectspace;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

/**
 *
 * @author krisada
 */
@Repository
public class RepositoryServiceImpl implements RepositoryService {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Override
    public int findNoiseword() {
        jdbcTemplate.query("SELECT noise_word FROM cfrm_exim_ic_re.lbk_noise_word ORDER BY LEN(noise_word) DESC", new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet rs) throws SQLException {
                SearchService.addCache(rs.getString(1));
            }
        });
        return SearchService.cache.size();
    }
}
