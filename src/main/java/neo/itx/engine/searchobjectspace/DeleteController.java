/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.searchobjectspace;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.ServletContext;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author krisada
 */
@RestController
public class DeleteController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private Environment env;

    @Autowired
    private DBService dbService;

    @RequestMapping(value = "/delete",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> execute(@RequestBody Map<String, Object> payload) {
        JSONObject resp = new JSONObject();
        resp.put("code", 200);
        resp.put("message", "success");
        try {
            Hashtable params = new Hashtable();
            String indexCoordinate = (String) payload.get("index");
            String templ = (String) payload.get("templ");
                Iterator its = payload.keySet().iterator();
                while (its.hasNext()) {
                    String key = (String) its.next();
                    params.put(key, payload.get(key));
                }
            
            for (Enumeration enums = params.keys(); enums.hasMoreElements();) {
                String key = (String) enums.nextElement();
                Object ov = params.get(key);
                if (ov.getClass().getName().equals("java.util.ArrayList")) {
                    log.info("Key:" + key + ", value:");
                    List lv = (List) ov;
                    for (int i = 0; i < lv.size(); i++) {
                        log.info("MATCHLIST :" + (String) lv.get(i) + ",");

                    }
                } else {
                    log.info("Key:" + key + ", value:" + (String) ov);
                }
            }
            log.info(String.format("host:%s", env.getProperty("elasticsearch.host")));
            File fileScript = new File(servletContext.getRealPath("/WEB-INF/scripts/" + templ + ".js"));
            ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
            engine.eval(new FileReader(fileScript));
            Invocable invocable = (Invocable) engine;
            String json = (String) invocable.invokeFunction("execute", params);
            log.info("JSON:" + json);
            String strJson = ElasticSearchClient.client().requestDelete(
                    env.getProperty("elasticsearch.host"), indexCoordinate, json);
            log.info("Response : " + strJson);
            
            //JSONArray rss = new JSONArray();
            JSONObject jsonTrans = new JSONObject(strJson);
            /*
            JSONObject _hits = jsonTrans.getJSONObject("hits");
            JSONArray hits = _hits.getJSONArray("hits");
            for (int i = 0; i < hits.length(); i++) {
                JSONObject os = hits.getJSONObject(i);
                JSONObject obj = os.getJSONObject("_source");
                double score = os.getDouble("_score");
                log.info(obj.toString());
                Map omp = obj.toMap();
                JSONObject nObj = new JSONObject();
                nObj.put("_score", score);
                Iterator ks = omp.keySet().iterator();
                while (ks.hasNext()) {
                    String k = (String) ks.next();
                    log.info("field:" + getFieldName(k));
                    nObj.put(getFieldName(k), omp.get(k));
                }
                rss.put(nObj);
            }*/
            resp.put("data", jsonTrans);
            log.info("Transaction Success..,");
        } catch (Exception ex) {
            resp.put("code", 500);
            resp.put("message", String.format("Transaction error:%s", ex.getMessage()));
            log.error("Error:", ex);
            ex.printStackTrace();
        }
        return new ResponseEntity<String>(resp.toString(), HttpStatus.OK);
    }

    private String getFieldName(String fname) {
        return fname.substring(fname.lastIndexOf("_") + 1, fname.length());
    }
}
