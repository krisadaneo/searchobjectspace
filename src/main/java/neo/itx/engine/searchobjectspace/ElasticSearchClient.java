/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.searchobjectspace;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author krisada
 */
public class ElasticSearchClient {

    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    private ElasticSearchClient() {
        
    }
    
    public static ElasticSearchClient client() {
        return new ElasticSearchClient();
    } 

    public String request(String host, String index, String jsonInputString) {
        try {
            URL url = new URL(String.format("http://%s:9200/%s/_search", host, index));

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            con.setRequestProperty("Content-Type", "application/x-ndjson; utf-8");
            con.setRequestProperty("Accept", "application/json");

            con.setDoOutput(true);

            try ( OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            int code = con.getResponseCode();
            log.info(String.format("Code : %d", code));

            try ( BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                return response.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public String requestDelete(String host, String index, String jsonInputString) {
        try {
            URL url = new URL(String.format("http://%s:9200/%s/_delete_by_query", host, index));

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            con.setRequestProperty("Content-Type", "application/x-ndjson; utf-8");
            con.setRequestProperty("Accept", "application/json");

            con.setDoOutput(true);

            try ( OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            int code = con.getResponseCode();
            log.info(String.format("Code : %d", code));

            try ( BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                return response.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    /*
    public static void main(String args[]) {
        ElasticSearchClient client = new ElasticSearchClient();
        System.out.println(client.request());
    }*/
}
